import java.util.Random;

public class Konsument implements Runnable 
{
	String alfabet;
	volatile Boolean czy_zlamane_haslo;
	volatile Boolean czy_koniec_hasel;
	int dlugosc_hasla;
	Bufor bufor;
	String haslo;
	Konsument(int dlugosc, Bufor bufor, String alfa) 
	{
		dlugosc_hasla=dlugosc;
		this.bufor=bufor;
		this.alfabet=alfa;
		this.haslo=haslogen1(dlugosc_hasla, alfabet);
		this.czy_zlamane_haslo=false;
		this.czy_koniec_hasel=false;
	}
	public void run()
	{
		System.out.println("Has�o: " + haslo);
		String message;
		while (true)
		{
			message= bufor.getMessage();
			System.out.println("Otrzymalem has�o: " + message);
			if(message.equals(""))
			{
				czy_koniec_hasel=true;
				System.out.println("czy_koniec_hasel: "+czy_koniec_hasel);
			}
			else if(message.equals(haslo))
			{
				czy_zlamane_haslo=true;
				System.out.println("czy_zlamane_haslo: "+czy_zlamane_haslo);
			}
			try
			{
				Thread.sleep(100);
			}
			catch (InterruptedException e) { }
		}
	}
	public String haslogen1(int n, String alfabet) 
	{
		String haslo="";
	    Random rand=new Random();
	    for(int i=0;i<n;i++)
	    {
	    	haslo=haslo+alfabet.charAt(rand.nextInt(alfabet.length()));
	    }
	    return haslo;
	}
}