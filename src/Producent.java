public class Producent implements Runnable
{
	String alfabet;
	StringBuffer haslo_buffer;
	int i=0;
	Boolean czy_wygenerowane_haslo;
	int nr_hasla;
	int ktore_wyslane;
	int dlugosc_hasla;
	Bufor bufor;
	Producent(int dlugosc, String alfa, Bufor b)
	{
		dlugosc_hasla=dlugosc;
		nr_hasla=0;
		ktore_wyslane=0;
		czy_wygenerowane_haslo=false;
		alfabet = alfa;
		haslo_buffer=new StringBuffer();
		bufor=b;
	}
	public void run() 
	{
		haslo_buffer.setLength(dlugosc_hasla);
		while (true) 
		{
			bufor.putMessage(this);
			try {
				
				Thread.sleep(100);
				}
			catch (InterruptedException e) { }
		}
	}
	public void produkuj_haslo()
	{
		if(ktore_wyslane<Math.pow(alfabet.length(), dlugosc_hasla))
		{
			czy_wygenerowane_haslo=false;
			nr_hasla=0;
			haslogen(dlugosc_hasla, alfabet.length(), 0, i);
			i=i+1;
			bufor.wiadomosci.add(haslo_buffer.toString());
			ktore_wyslane=ktore_wyslane+1;
		}
		else
		{
			bufor.wiadomosci.add("");
		}
	}
	void haslogen(int n, int L, int level, int ktore) 
	{
	    if (level == n) 
	    { 
	    	if(ktore==nr_hasla)
    		{
	    		czy_wygenerowane_haslo=true;
	    		return;
    		}
	    	nr_hasla+=1;
	    }
	    else 
	    { 
	    	for (int i=0;i<L;i++) 
	    	{
	    		haslo_buffer.setCharAt(level, alfabet.charAt(i));    
	        	haslogen(n,L,level+1,ktore);
	        	if(czy_wygenerowane_haslo==true) return;
	    	}
	    }
	}
}