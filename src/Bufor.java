import java.util.ArrayList;
import java.util.List;

public class Bufor 
{
	static final int MAXQUEUE = 5;
	List wiadomosci;
	Bufor()
	{
		wiadomosci = new ArrayList();
	}
	synchronized void putMessage(Producent p) 
	{
		while ( wiadomosci.size() >= MAXQUEUE )
			try 
			{
				System.out.println("Pe�na lista");
				wait();
			}
		catch(InterruptedException e) { }
		p.produkuj_haslo();
		notify();
	}
	public synchronized String getMessage() 
	{
		while (wiadomosci.size() == 0)
			try 
			{
				System.out.println("Pusta lista");
				notify();
				wait();
			}
		catch(InterruptedException e) { }
		String message = (String)wiadomosci.remove(0);
		notify();
		return message;
	}
}