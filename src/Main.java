public class Main 
{
	@SuppressWarnings("deprecation")
	public static void main(String[] args) 
	{
		int dlugosc_hasla=3;
		String alfabet="abc";
		Bufor bufor=new Bufor();
		Producent producent = new Producent(dlugosc_hasla,alfabet,bufor);
		Thread prod=new Thread(producent);
		Konsument konsument = new Konsument(dlugosc_hasla, bufor, alfabet);
		Thread kons=new Thread(konsument);
		prod.start();
		kons.start();
		while(konsument.czy_zlamane_haslo==false && konsument.czy_koniec_hasel==false);
		prod.stop();
		kons.stop();
		System.out.println("Koniec programu");
	}
}
